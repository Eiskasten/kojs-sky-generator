package ch.cheapbit.kojsskygen.common;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

/**
 * Created by richi on 30.12.16.
 */

@Mod(modid = KojsSkyGenMod.MODID, version = KojsSkyGenMod.VERSION)
public class KojsSkyGenMod {
    public static final String MODID = "kojsskygen";
    public static final String VERSION = "0.0.1";

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        KojsSkyType.register();
    }
}
