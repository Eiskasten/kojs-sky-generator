package ch.cheapbit.kojsskygen.common.villages;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by richi on 02.01.17.
 */
public class PathGroundArtist {

    public static void toImage(PathGround pg, File image) {
        BufferedImage bi = new BufferedImage(pg.getSize(), pg.getSize(), BufferedImage.TYPE_INT_RGB);
        for (int z = 0; z < pg.getSize(); z++) {
            for (int x = 0; x < pg.getSize(); x++) {
                int color = 0xffffff;

                byte wayData = pg.getData(x, z);

                if ((wayData & PathGround.PATH) == PathGround.PATH)
                    color = 0x000000;
                if ((wayData & PathGround.CROSS) == PathGround.CROSS)
                    color = 0x00ff00;
                if ((wayData & PathGround.POINT) == PathGround.POINT)
                    color = 0xff0000;
                if ((wayData & PathGround.FOUNTAIN) == PathGround.FOUNTAIN)
                    color = 0x0000ff;
                bi.setRGB(x, z, color);
            }
        }
        try {
            ImageIO.write(bi, "gif", image);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
