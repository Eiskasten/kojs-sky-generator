package ch.cheapbit.kojsskygen.common.villages;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by richi on 31.12.16.
 */
public class Path {

    //size in tiles
    private int size;
    private PathTile[][] field;

    public Path(File tileFile, Random rand) {
        size = 8;
        field = new PathTile[size][];
        for (int i = 0; i < size; i++)
            field[i] = new PathTile[size];
        List<PathTile> tiles = PathTile.fromFile(tileFile);
        for (int z = 0; z < size; z++) {
            for (int x = 0; x < size; x++) {

                do {
                    field[z][x] = randomAccess(tiles, rand);
                } while (!tileFits(x, z));
            }
        }
    }

    private PathTile randomAccess(List<PathTile> list, Random rand) {
        return list.get(rand.nextInt(list.size()));
    }

    private boolean tileFits(int x, int z) {
        //TODO check if tile fits with others
        PathTile checkingTile = get(x, z);
        PathTile northTile = null;
        PathTile westTile = null;
        if (z > 0)
            northTile = get(x, z - 1);
        if (x > 0)
            westTile = get(x - 1, z);
        boolean ret = true;
        ret &= (northTile == null || checkingTile.fit(northTile, PathTile.NORTH));
        ret &= (westTile == null || checkingTile.fit(westTile, PathTile.WEST));
        return ret;
    }

    public PathTile get(int x, int z) {
        x %= size;
        z %= size;
        return field[z][x];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int z = 0; z < size; z++) {
            for (int chZ = 0; chZ < PathTile.SIZE; chZ++) {
                for (int x = 0; x < size; x++) {
                    sb.append(field[z][x].toXString(chZ));
                }
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}
