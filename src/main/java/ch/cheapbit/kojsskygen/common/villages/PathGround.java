package ch.cheapbit.kojsskygen.common.villages;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by richi on 01.01.17.
 */
public class PathGround {

    public static final byte VOID = 0;
    public static final byte PATH = 0b1;
    public static final byte CROSS = 0b10;
    public static final byte FOUNTAIN = 0b100;
    public static final byte POINT = 0b100000;

    public static final char START_CHAR = '#';

    private int size;
    private int paths;
    private byte[][] field;
    private Random random;

    public PathGround(int size, Random random) {
        this.size = size;
        this.random = random;

        field = new byte[size][];
        for (int i = 0; i < size; i++)
            field[i] = new byte[size];

        setFountain();

    }

    public PathGround(int size) {
        this(size, new Random());

    }

    public PathGround(Random random) {
        this(16, random);
    }

    public PathGround() {
        this(new Random());
    }

    public void beginDraw() {
        ArrayList<Coord> lines = new ArrayList<>();
        for (int i = 0; i < 4; i++)
            lines.add(new Coord(size / 2, size / 2));

        while (lines.size() > 0) {
            lines.addAll(lines.get(0).drawLine());
            lines.remove(0);
        }
    }

    private void setFountain() {
        /*for (int z = 0; z < size; z++)
            for (int x = 0; x < size; x++)
                setData(x, z, VOID);*/
        setData(size / 2, size / 2, (byte) (PATH | FOUNTAIN | CROSS));
    }

    private void setData(int x, int z, byte data) {
        x %= size;
        z %= size;
        field[z][x] |= data;
    }

    public byte getData(int x, int z) {
        x %= size;
        z %= size;
        return field[z][x];
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (byte[] x : this.field) {
            for (byte fieldData : x) {
                sb.append(convertFieldData(fieldData));
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    private char convertFieldData(byte data) {
        if (data == VOID)
            return (char) 0x25a9;
        if ((data & FOUNTAIN) == FOUNTAIN)
            return (char) 0x2299;
        if ((data & POINT) == POINT)
            return (char) 0x229A;
        if ((data & PATH) == PATH)
            return (char) 0x25cf;
        if ((data & CROSS) == CROSS)
            return (char) 0x2297;
        return (char) 0x25cd;
    }

    private class Coord {
        private int x, z;

        private Coord(int x, int z) {
            this.x = x;
            this.z = z;
        }

        private List<Coord> drawLine() {
            int xState;
            int zState;

            int newX = x;
            int newZ = z;

            do {
                xState = random.nextInt(3) - 1;
                zState = random.nextInt(3) - 1;
            } while (((getData(x + xState, z + zState) & PATH) != 0) || ((xState == 0) == (zState == 0)));

            int length = (int) Math.sqrt(size);
            length += random.nextInt(length);

            for (int i = 1; i <= length; i++) {
                if (!isValidPosition(newX, newZ)) {
                    newX -= xState;
                    newZ -= zState;
                    break;
                }

                setData(newX, newZ, PATH);

                newX += xState;
                newZ += zState;
            }

            setData(newX, newZ, CROSS);

            int newPaths = 0;

            ArrayList<Coord> ret = new ArrayList<>();

            if (paths <= size / 2) {
                double rand = Math.sqrt(random.nextInt(100));
                if (rand >= 6)
                    newPaths = 1;
                else if (rand >= 2)
                    newPaths = 2;

                for (int i = 0; i < newPaths; i++)
                    ret.add(new Coord(newX, newZ));
            }
            paths++;
            return ret;
        }

        private boolean isValidPosition(int x, int z) {
            int pathCount = 0;
            pathCount += addValidInvalid(x + 1, z);
            pathCount += addValidInvalid(x + 2, z);
            pathCount += addValidInvalid(x - 1, z);
            pathCount += addValidInvalid(x - 2, z);

            pathCount += addValidInvalid(x, z + 1);
            pathCount += addValidInvalid(x, z + 2);
            pathCount += addValidInvalid(x, z - 1);
            pathCount += addValidInvalid(x, z - 2);

            return pathCount <= 2;
        }

        private int addValidInvalid(int x, int z) {
            if ((getData(x, z) & PATH) == PATH)
                return 1;
            return 0;
        }
    }
}
