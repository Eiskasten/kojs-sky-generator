package ch.cheapbit.kojsskygen.common.villages;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by richi on 31.12.16.
 */
public class PathTile {

    public static final byte VOID = 1;
    public static final byte PATH = 0b10;
    public static final byte POINT = 0b100;

    public static final int SIZE = 4;

    public static final byte EAST = 0;
    public static final byte NORTH = 1;
    public static final byte WEST = 2;
    public static final byte SOUTH = 3;

    public static final char START_CHAR = '#';

    private byte[][] field;

    public PathTile() {
        field = emptyField();
    }

    public byte get(int x, int z) {
        x %= SIZE;
        z %= SIZE;
        return field[z][x];
    }

    public String toXString(int z) {
        z %= SIZE;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < SIZE; i++)
            sb.append((char) (START_CHAR + get(i, z)));
        return sb.toString();
    }

    public static byte[][] emptyField() {
        return new byte[][]{{VOID, VOID, VOID, VOID}, {VOID, VOID, VOID, VOID}, {VOID, VOID, VOID, VOID}, {VOID, VOID, VOID, VOID}};
    }

    public static PathTile emptyTile() {
        PathTile ret = new PathTile();
        ret.field = emptyField();
        return ret;
    }

    public boolean fit(PathTile other, byte otherDirection) {
        byte[] thisEdge = getEdge(otherDirection);
        byte[] otherEdge = other.getEdge(getOpposite(otherDirection));

        boolean ret = true;
        for (int i = 0; i < SIZE && ret; i++)
            ret &= (thisEdge[i] & otherEdge[i]) > 0;

        return ret;
    }

    private byte[] getEdge(byte direction) {
        byte[] ret = new byte[SIZE];
        switch (direction) {
            case NORTH:
                ret = this.field[0];
                break;
            case SOUTH:
                ret = this.field[SIZE - 1];
                break;
            case EAST: {
                for (int i = 0; i < SIZE; i++) {
                    ret[i] = this.field[i][SIZE - 1];
                }
                break;
            }
            case WEST: {
                for (int i = 0; i < SIZE; i++) {
                    ret[i] = this.field[i][0];
                }
                break;
            }
        }
        return ret;
    }

    public boolean hasEdgeData(byte pathData, byte edgePosition) {
        boolean ret = false;
        for (byte point : getEdge(edgePosition))
            ret |= (point & pathData) > 0;
        return ret;
    }

    private static byte getOpposite(byte direction) {
        return (byte) ((direction + 2) % 4);
    }

    public static List<PathTile> fromFile(File file) {
        ArrayList<PathTile> ret = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                if (line.length() == SIZE) {
                    sb.append(line);
                    sb.append(START_CHAR);
                } else {
                    if (sb.length() == SIZE * (SIZE + 1)) {
                        ret.add(fromString(sb.toString()));
                        sb = new StringBuilder();
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static PathTile fromString(String pattern) {
        PathTile ret = new PathTile();
        int z = 0;
        for (String line : pattern.split(START_CHAR + "")) {
            int x = 0;
            for (char fieldData : line.toCharArray()) {
                if (x >= SIZE)
                    break;
                ret.field[z][x] = (byte) (fieldData - START_CHAR);
                x++;
            }
            z++;
        }
        return ret;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (byte[] x : this.field) {
            for (byte fieldData : x) {
                sb.append((char) (START_CHAR + fieldData));
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
