package ch.cheapbit.kojsskygen.common;

import jline.internal.Log;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraftforge.fml.common.FMLLog;

/**
 * Created by richi on 30.12.16.
 */
public class KojsSkyType extends WorldType {
    /**
     * Creates a new world type, the ID is hidden and should not be referenced by modders.
     * It will automatically expand the underlying workdType array if there are no IDs left.
     *
     * @param name
     */
    public static final String FANCY_NAME = "Kojs Sky";
    public static final String INTERNAL_NAME = "kojssky";

    public KojsSkyType(String name) {
        super(name);
        registerWorldType();
    }

    private void registerWorldType() {
        WorldType[] types = WORLD_TYPES;
        for (int i = 0; i < types.length; i++) {
            if (types[i] == null) {
                types[i] = this;
                break;
            }
        }
    }

    public static void register() {
        new KojsSkyType(FANCY_NAME);
        FMLLog.info("Registered " + FANCY_NAME + " as a world type");
    }

    @Override
    public boolean isCustomizable() {
        return true;
    }

    @Override
    public boolean getCanBeCreated() {
        return true;
    }

    @Override
    public String getTranslateName() {
        return "Kojs Sky";
    }

    @Override
    public IChunkGenerator getChunkGenerator(World world, String generatorOptions) {
        return new EmptyChunkGenerator(world, world.getSeed(), world.getWorldInfo().isMapFeaturesEnabled(), generatorOptions);
    }
}
