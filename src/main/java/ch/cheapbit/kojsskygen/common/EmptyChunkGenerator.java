package ch.cheapbit.kojsskygen.common;

import ch.cheapbit.kojsskygen.common.villages.MapGenVillageIsland;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkPrimer;
import net.minecraft.world.gen.ChunkProviderOverworld;
import net.minecraft.world.gen.ChunkProviderSettings;

/**
 * Created by richi on 30.12.16.
 */
public class EmptyChunkGenerator extends ChunkProviderOverworld {

    private ChunkProviderSettings settings;
    private final boolean mapFeaturesEnabled;
    private MapGenVillageIsland villageGenerator;
    private final World world;

    public EmptyChunkGenerator(World worldIn, long seed, boolean mapFeaturesEnabledIn, String p_i46668_5_) {
        super(worldIn, seed, mapFeaturesEnabledIn, p_i46668_5_);

        this.mapFeaturesEnabled = mapFeaturesEnabledIn;
        this.world = worldIn;

        this.villageGenerator = new MapGenVillageIsland();

        if (p_i46668_5_ != null) {
            this.settings = ChunkProviderSettings.Factory.jsonToFactory(p_i46668_5_).build();
            worldIn.setSeaLevel(this.settings.seaLevel);
        }
    }

    @Override
    public void setBlocksInChunk(int x, int z, ChunkPrimer primer) {
    }

    @Override
    public void replaceBiomeBlocks(int x, int z, ChunkPrimer primer, Biome[] biomesIn) {
    }

    @Override
    public void recreateStructures(Chunk chunkIn, int x, int z) {
        if (this.mapFeaturesEnabled) {

            if (this.settings.useVillages) {
                this.villageGenerator.generate(this.world, x, z, (ChunkPrimer) null);
            }
        }
    }
}
