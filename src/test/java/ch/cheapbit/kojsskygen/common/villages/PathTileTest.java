package ch.cheapbit.kojsskygen.common.villages;

import org.junit.Test;

import java.io.File;
import java.util.Random;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by richi on 31.12.16.
 */
public class PathTileTest {

    @Test
    public void baum() {
        System.out.println(PathTile.fromString("$$$$#$$$$#''''#$$$$"));
        Path p = new Path(new File("src/main/resources/pathtiles/default"), new Random());
        System.out.println(p);
    }

    @Test
    public void baum2(){
        PathTile p1 = PathTile.fromString("$$'$#$$'$#''''#$$$$");
        PathTile p2 = PathTile.fromString("$$'$#$$'$#$'''#$$$$");
        System.out.println(p1);
        System.out.println(p2);
        assertThat(p2.fit(p1, PathTile.WEST), equalTo(false));
    }
}
