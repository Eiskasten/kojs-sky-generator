package ch.cheapbit.kojsskygen.common.villages;

import org.junit.Test;
import org.lwjgl.Sys;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.Path;
import java.util.Random;

/**
 * Created by richi on 01.01.17.
 */
public class PathGroundTest {

    @Test
    public void drawSth() {
        PathGround p = new PathGround(new Random(5));
        p.beginDraw();
        try {
            Path path = Files.createTempFile("path","gif");
            PathGroundArtist.toImage(p, path.toFile());
            System.out.println(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
